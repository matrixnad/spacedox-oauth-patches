SpaceDox feature integration
=====================================================

This repository contains:

```
AES Encrypytion integration
OAuth code parameter patch 
```

Testing with Visual Studio
----------------------------------------------------

please open the .sln with Visual Studio, and test an example

Testing encryption 

The tests/TestAES.cs, OAuth token results should produce an encrypted and decrypted cipher. From
and to the database

Testing OAuth code Security Problem
 
The OAuth 'code' parameter flow 

In the tests/TestCodeCallback.cs the test should make the code parameter invisible
to the user.


