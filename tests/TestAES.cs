
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleAES;
namespace Program {
	class Encryption {	
		static void Main(string[] args) {
			SimpleAES aes = new SimpleAES;					
			Console.WriteLine("Encryption value:"):
			string encrypted = aes.EncryptToString("OAUTH_TOKEN");
			Console.WriteLine(encrypted);
			string decrypted = aes.DecryptString(encrypted);
			Console.WriteLine("Decrypted");
			Console.WriteLine(decrypted);
		}
	}
}
